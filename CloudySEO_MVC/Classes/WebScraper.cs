﻿using System;
using System.Collections.Concurrent;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Newtonsoft.Json;
using CloudySEO_MVC.Classes.CategoryCheckers;

namespace CloudySEO_MVC.Classes
{
    public class WebScraper
    {
        private IWebDriver driver;
        private string url;
        private string initialSanitizedURL;
        private ConcurrentDictionary<Uri, ResponseData> responseData;

        /// <summary>Initializes a new instance of the <c>WebScraper</c> class.</summary>
        public WebScraper(string url)
        {
            ChromePerformanceLoggingPreferences logPrefs = new ChromePerformanceLoggingPreferences(); // based loosely off of Stackoverflow Java selenium answer (lookup 'selenium status code')
            logPrefs.AddTracingCategories(new string[] { "devtools.timeline" }); // based on seleniumHQ issue 1844
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.PerformanceLoggingPreferences = logPrefs;
            chromeOptions.SetLoggingPreference("performance", LogLevel.All);
            chromeOptions.AddArguments("--headless", "--no-sandbox");
            string sanitizedURL = sanitizeURL(url);
            initialSanitizedURL = sanitizedURL;
            System.Diagnostics.Debug.WriteLine(sanitizedURL);
            driver = new ChromeDriver(chromeOptions)
            {
                Url = sanitizedURL
            };
            this.url = driver.Url;
            responseData = new ConcurrentDictionary<Uri, ResponseData> { };
            getResponseData();
        }


        /// <summary>method <c>HasFinalOKResponse</c> returns true if the final status code for the resolved url was 200 OK.</summary>
        public bool HasFinalOKResponse()
        {
            int statusCode = 999;
            try
            {
                statusCode = responseData[new Uri(url)].statusCode;
            }
            catch (Exception ex)
            {

            }
            return statusCode == 200;
        }


        /// <summary>method <c>isTextHTMLContentType</c> returns true if the webpage is text/html.</summary>
        public bool isTextHTMLContentType()
        {
            //Content - type: text / html
            bool isHTML = false;
            try
            {
                isHTML = responseData[new Uri(url)].isHTMLDocument();
            }
            catch (Exception ex)
            {

            }
            return isHTML;
        }


        /// <summary>method <c>getResponseData</c> parses Chrome Driver performance logs and stores responseData in the instanced responseData ConcurrentDict.</summary>
        public void getResponseData()
        {
            foreach(LogEntry log in driver.Manage().Logs.GetLog("performance")){

                System.Diagnostics.Debug.WriteLine(log);
                dynamic logDeserialized = JsonConvert.DeserializeObject(log.Message);
                dynamic message = logDeserialized.message;
                string method = message.method; 
                if (method != null && "Network.responseReceived".Equals(method))
                {
                    dynamic parameters = message["params"];
                    dynamic response = parameters.response;
                    string responseURL = response.url;
                    System.Diagnostics.Debug.WriteLine($"[response url: '{responseURL}', url: {url}");
                    int statusCode = response.status;
                    int datalength = response.encodedDataLength;
                    if (!responseData.ContainsKey(new Uri(responseURL)))
                        responseData.GetOrAdd(new Uri(responseURL), new ResponseData(responseURL, statusCode, response.headers, datalength));
                } else if (method != null && "Network.requestWillBeSent".Equals(method)) // https://stackoverflow.com/questions/2068418/whats-the-difference-between-a-302-and-a-307-redirect
                {
                    dynamic parameters = message["params"];
                    dynamic response = parameters.redirectResponse;
                    if (response != null)
                    {
                        int statusCode = response.status;
                        string responseURL = response.headers.Location;
                        string initialURL = response.url;

                        System.Diagnostics.Debug.WriteLine($"[redirected-to response url: '{responseURL}', url: {url}");
                        if (!responseData.ContainsKey(new Uri(initialURL)))
                            responseData.GetOrAdd(new Uri(initialURL), new ResponseData(responseURL, statusCode, response.headers, 0));
                    }
                }
            }
        }


        /// <summary>method <c>sanitizeURL</c> normalizes the url, making sure it has a proper scheme. Assumes if no scheme that the uri is for an https webpage.</summary>
        public string sanitizeURL(string initialURL)
        {
            Uri uri = new UriBuilder(initialURL).Uri;
            if(uri.Scheme == "http" && !initialURL.StartsWith("http"))
            {
                return "https://" + initialURL;
            }
            else
            {
                return initialURL;
            }
        }


        /// <summary>method <c>~WebScraper</c> quits the chrome webdriver.</summary>
        ~WebScraper() {
            try
            {
                driver.Quit();
            } catch(Exception ex)
            {

            }
        }

        /// <summary>method <c>ContentCheck</c> returns the RankingCategory data from the underlying checker.</summary>
        public RankingCategory ContentCheck()
        {
            return ContentChecker.contentCategory(responseData, initialSanitizedURL, url, driver);
        }

        /// <summary>method <c>LinksCheck</c> returns the RankingCategory data from the underlying checker.</summary>
        public RankingCategory LinksCheck()
        {
            return LinksChecker.LinksCheck(driver, responseData, url);
        }

        /// <summary>method <c>ImagesCheck</c> returns the RankingCategory data from the underlying checker.</summary>
        public RankingCategory ImagesCheck()
        {
            return ImagesChecker.ImagesCheck(driver, responseData, url);
        }

        /// <summary>method <c>OptimizationCheck</c> returns the RankingCategory data from the underlying checker.</summary>
        public RankingCategory OptimizationCheck()
        {
            return OptimizationChecker.optimizationCategory(initialSanitizedURL, url, driver, responseData);
        }
    }
}
