﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CloudySEO_MVC.Classes
{
    public class MultivaluedRankingFactor
    {
        public string title;
        public string meaning;
        public string status;
        public ConcurrentBag<List<RankingFactor>> factorList;


        /// <summary>Initializes a new instance of the <c>MultivaluedRankingFactor</c> class.</summary>
        public MultivaluedRankingFactor(string title)
        {
            this.title = title;
            factorList = new ConcurrentBag<List<RankingFactor>>();
        }

        /// <summary>Initializes a new instance of the <c>MultivaluedRankingFactor</c> class.</summary>
        public MultivaluedRankingFactor(string title, string meaning)
        {
            this.title = title;
            this.meaning = meaning;
            factorList = new ConcurrentBag<List<RankingFactor>>();
        }
        
    }
}
