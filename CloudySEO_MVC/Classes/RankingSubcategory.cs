﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class RankingSubcategory
    {
        public string title;
        public HashSet<RankingFactor> factors;
        public HashSet<MultivaluedRankingFactor> multivaluedFactors;

        /// <summary>Initializes a new instance of the <c>RankingSubcategory</c> class.</summary>
        public RankingSubcategory(string title)
        {
            this.title = title;
            factors = new HashSet<RankingFactor> { };
            multivaluedFactors = new HashSet<MultivaluedRankingFactor> { };
    }
    }
}
