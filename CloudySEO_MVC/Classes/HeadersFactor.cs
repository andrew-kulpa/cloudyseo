﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class HeadersFactor : MultivaluedRankingFactor
    {

        /// <summary>Initializes <c>HeadersFactor</c> and uses base constructor of MultivaluedRankingFactor.</summary>
        public HeadersFactor(string title, string meaning) : base(title, meaning)
        {

        }

        /// <summary>method <c>addHeader</c> adds the header as a RankingFactor to a list of headers.</summary>
        public void addHeader(string header, string value)
        {
            List<RankingFactor> headerFactors = new List<RankingFactor> { };
            headerFactors.Add(new RankingFactor("Header", header) { status = "info" });
            headerFactors.Add(new RankingFactor("Value", value) { status = "info" });
            this.factorList.Add(headerFactors);
        }

    }
}
