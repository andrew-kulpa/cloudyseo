﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class LinksFactor : MultivaluedRankingFactor
    {
        /// <summary>method <c>LinksFactor</c> uses base constructor of MultivaluedRankingFactor.</summary>
        public LinksFactor(string title, string meaning) : base(title, meaning)
        {

        }
        
        /// <summary>method <c>addLink</c> adds the link as a RankingFactor to a list of links.</summary>
        public void addLink(string tag, string url, string content, int statusCode)
        {
            List<RankingFactor> linkFactors = new List<RankingFactor> { };
            linkFactors.Add(tagFactor(tag));
            linkFactors.Add(contentFactor(content));
            linkFactors.Add(urlFactor(url));
            linkFactors.Add(statusCodeFactor(statusCode));
            this.factorList.Add(linkFactors);
        }

        /// <summary>method <c>tagFactor</c> returns an informational RankingFactor.</summary>
        public RankingFactor tagFactor(string tag)
        {
            return new RankingFactor("tag", tag)
            {
                status = "info"
            };
        }

        /// <summary>method <c>urlFactor</c> returns an informational RankingFactor.</summary>
        public RankingFactor urlFactor(string url)
        {
            return new RankingFactor("url", url)
            {
                status = "info"
            };
        }

        /// <summary>method <c>contentFactor</c> returns a RankingFactor with a status set to bad if the content is empty.</summary>
        public RankingFactor contentFactor(string content)
        {
            string status;
            if (content == null || content.Trim().Equals(""))
            {
                status = "bad";
            }
            else
            {
                status = "good";
            }
            return new RankingFactor("content", content)
            {
                status = status
            };
        }

        /// <summary>method <c>statusCodeFactor</c> returns a RankingFactor with a status of good if 200, warning if 300-399, and bad otherwise.</summary>
        public RankingFactor statusCodeFactor(int statusCode)
        {
            string status;
            if (statusCode == 200)
            {
                status = "good";
            }
            else if (statusCode > 299 && statusCode < 400)
            {
                status = "warning";
            }
            else
            {
                status = "bad";
            }
            return new RankingFactor("status code", statusCode.ToString())
            {
                status = status
            };
        }
    }
}
