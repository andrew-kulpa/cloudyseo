﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class RankingCategory
    {
        public string title;
        public HashSet<RankingSubcategory> subcategories;
        public Dictionary<string, string> googleSnippet;


        /// <summary>Initializes a new instance of the <c>RankingCategory</c> class.</summary>
        public RankingCategory(string title)
        {
            this.title = title;
            subcategories = new HashSet<RankingSubcategory> { };
        }
    }
}
