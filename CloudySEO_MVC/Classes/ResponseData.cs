﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class ResponseData
    {
        public string url;
        public int statusCode;
        public dynamic headers;
        public int datalength;


        /// <summary>Initializes a new instance of the <c>ResponseData</c> class.</summary>
        public ResponseData(string url, int statusCode, dynamic headers, int datalength)
        {
            this.url = url;
            this.statusCode = statusCode;
            this.headers = headers;
            this.datalength = datalength;
        }

        /// <summary>method <c>Server</c> returns the header info from the response headers.</summary>
        public String Server()
        {
            return headers.server;
        }

        /// <summary>method <c>getHeaders</c> returns the header info.</summary>
        public Dictionary<string, string> getHeaders()
        {
            return new Dictionary<string, string>(headers.ToObject<IDictionary<string, string>>(), StringComparer.CurrentCultureIgnoreCase);
        }
        
        /// <summary>method <c>ContentEncoding</c> returns the content encoding or null if not found.</summary>
        public String ContentEncoding()
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>(headers.ToObject<IDictionary<string, object>>(), StringComparer.CurrentCultureIgnoreCase);
                return (string)d["content-encoding"];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>method <c>isHTMLDocument</c> returns whether the resource was an HTML document based on the content-type header info.</summary>
        public bool isHTMLDocument()
        {
            try
            {
                Dictionary<string, object> d = new Dictionary<string, object>(headers.ToObject<IDictionary<string, object>>(), StringComparer.CurrentCultureIgnoreCase);

                string content_type_debug_message = "content-type for base resolved url (" + url + ") :" + (string)d["content-type"];
                System.Diagnostics.Debug.WriteLine(content_type_debug_message);
                String contentType = (string)d["content-type"];
                return contentType.IndexOf("text/html", StringComparison.OrdinalIgnoreCase) >= 0;
            } catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception in isHTMLDocument():" + ex.Message);
                return false;
            }
        } 

    }
}
