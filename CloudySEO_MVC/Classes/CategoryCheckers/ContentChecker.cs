﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudySEO_MVC.Classes;
using System.Collections.Concurrent;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Text.RegularExpressions;
using System.Configuration;

namespace CloudySEO_MVC.Classes.CategoryCheckers
{
    public class ContentChecker : CategoryChecker
    {

        /// <summary>method <c>contentCategory</c> generates RankingCategory data for the main page content.</summary>
        public static RankingCategory contentCategory(ConcurrentDictionary<Uri, ResponseData> responseData, string initialSanitizedURL, string url, IWebDriver driver)
        {
            RankingCategory content = new RankingCategory("Content");

            RankingSubcategory statusChecks = new RankingSubcategory("Status Checks");
            statusChecks.factors.Add(checkStatusCode(responseData, initialSanitizedURL));
            statusChecks.factors.Add(getDocumentLength(responseData, url)); // TODO: Check full document size
            statusChecks.factors.Add(compressionFactor(responseData,  url));
            content.subcategories.Add(statusChecks);

            RankingSubcategory titleChecks = new RankingSubcategory("Title Checks");
            titleChecks.factors.Add(getTitleInfoFactor(driver));
            titleChecks.factors.Add(getTitleLengthFactor(driver)); // TODO: Check title length
            content.subcategories.Add(titleChecks);


            RankingSubcategory descriptionChecks = new RankingSubcategory("Description Checks");
            descriptionChecks.factors.Add(getDescriptionInfoFactor(driver));
            descriptionChecks.factors.Add(getDescriptionLengthFactor(driver)); // TODO: Check description length
            content.subcategories.Add(descriptionChecks);

            RankingSubcategory headingChecks = new RankingSubcategory("Heading");
            headingChecks.factors.Add(GetHeading1InfoFactor(driver));
            headingChecks.factors.Add(GetHeading1CountFactor(driver));
            headingChecks.multivaluedFactors.Add(GetHeadingsFactor(driver));
            content.subcategories.Add(headingChecks);


            RankingSubcategory contentChecks = new RankingSubcategory("Content");
            contentChecks.factors.Add(getContentLengthFactor(driver));
            contentChecks.factors.Add(getCodeToTextRatioFactor(driver));
            contentChecks.multivaluedFactors.Add(getTopWordsUsed(driver));
            content.subcategories.Add(contentChecks);

            content.googleSnippet = GetGoogleSnippet(url, driver);

            return content;
        }

        /// <summary>method <c>checkStatusCode</c> generates RankingFactor data related to status code returned back for the user's (sanitized) input url.</summary>
        public static RankingFactor checkStatusCode(ConcurrentDictionary<Uri, ResponseData> responseData, string initialSanitizedURL)
        {
            RankingFactor statusCode = new RankingFactor();
            statusCode.title = "Status Code";
            int statusnumber = 999;
            if (responseData.ContainsKey(new Uri(initialSanitizedURL)))
            {
                statusnumber = responseData[new Uri(initialSanitizedURL)].statusCode;
            }
            else
            {

                System.Diagnostics.Debug.WriteLine("url not found in responseData Dictionary: " + initialSanitizedURL);
                System.Diagnostics.Debug.WriteLine("Dictionary: " + responseData.ToString());
            }
            System.Diagnostics.Debug.WriteLine("status number: " + statusnumber);
            statusCode.value = statusnumber.ToString();
            statusCode.meaning = Startup.config["Factor_Meanings:Content:Status:Status_Code"];
            if (statusnumber == 200)
            {
                statusCode.status = "good";
            }
            else if (statusnumber > 299 && statusnumber < 400)
            {
                statusCode.status = "warning";
            }
            else
            {
                statusCode.status = "bad";
            }
            return statusCode;
        }

        /// <summary>method <c>getDocumentLength</c> generates RankingFactor data related to data length returned back via the first response data headers of the final resolved url.</summary>
        public static RankingFactor getDocumentLength(ConcurrentDictionary<Uri, ResponseData> responseData, string url)
        {
            int sizeInBytes = responseData[new Uri(url)].datalength;

            RankingFactor contentLengthFactor = new RankingFactor("Document Size")
            {
                value = $"{sizeInBytes} bytes",
                meaning = Startup.config["Factor_Meanings:Content:Status:Document_Length"]
            };

            if (sizeInBytes > 10000)
            {
                contentLengthFactor.status = "bad";
            }
            else
            {
                contentLengthFactor.status = "good";
            }

            return contentLengthFactor;
        }

        /// <summary>method <c>getTitleInfoFactor</c> generates RankingFactor data related to the title of the page.</summary>
        public static RankingFactor getTitleInfoFactor(IWebDriver driver)
        {
            string status = "info";
            string meaningPrefix = "";

            if (driver.Title == null || driver.Title == "")
            {
                status = "bad";
                meaningPrefix = "No title was found for the given webpage.\n";
            }
            else if (isGenericTitle(driver.Title)) // based on https://support.google.com/webmasters/answer/35624?hl=en "Create good titles and snippets in Search Results"
            {
                status = "warning";
                meaningPrefix = "The title of the webpage is generic.\n";
            }

            if (driver.FindElements(By.XPath("//title")).Count > 1)
            {
                status = "bad";
                meaningPrefix += "More than one title was found on the given webpage.\n";
            }

            RankingFactor titleInfoFactor = new RankingFactor("Title")
            {
                value = driver.Title,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Content:Title:Info"]
        };
            return titleInfoFactor;
        }


        /// <summary>method <c>getTitleLengthFactor</c> generates RankingFactor data related to the length of the title of the page.</summary>
        public static RankingFactor getTitleLengthFactor(IWebDriver driver)
        {
            string status;
            string messagePrefix = "";
            if (driver.Title.Length > 60)
            {
                status = "bad";
                messagePrefix = "Your title is too long!\n";
            }
            else
            {
                status = "good";
            }
            RankingFactor titleLengthFactor = new RankingFactor("Length")
            {
                value = driver.Title.Length.ToString() + " characters; " + driver.Title.Trim().Split(' ').Count() + " Words",
                status = status,
                meaning = messagePrefix + Startup.config["Factor_Meanings:Content:Title:Length"]
            };
            return titleLengthFactor;
        }


        /// <summary>method <c>isGenericTitle</c> returns back true if the title is found within a simple generic title list.</summary>
        public static bool isGenericTitle(string title)
        {
            var genericTitles = Startup.config.GetSection("Generic_Titles").GetChildren().ToList().Select(x => x.ToString());
            return genericTitles.Contains(title.ToLower());
        }

        /// <summary>method <c>GetDescription</c> returns the site meta description as null or a string.</summary>
        public static String GetDescription(IWebDriver driver)
        {
            return GetMetaTagInformation("description", driver);
        }

        /// <summary>method <c>isKeywordStuffingDescription</c> returns true if any word in the description shows up more than 8 times and are longer than 5 chars.</summary>
        public static bool isKeywordStuffingDescription(string description)
        {
            String[] words = Regex.Split(description, @"(,|.|!|\'s||'|\s)",RegexOptions.IgnoreCase);
            if(words.Length < 8)
            {
                return false;
            }
            Dictionary<String, int> word_counts = new Dictionary<String, int> { };
            int max_occurrences = 1;
            String max_word = words[0];
            foreach(string word in words)
            {
                if(word.Length < 5) // skip too short of words
                {
                    continue;
                }

                if (word_counts.ContainsKey(word)) {
                    word_counts[word] = word_counts[word] + 1;
                    if(word_counts[word] > word_counts[max_word])
                    {
                        max_occurrences = word_counts[word];
                        max_word = word;
                    }
                } else {
                    word_counts[word] = 1;
                }
            }

            if(max_occurrences > 8)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>method <c>isKeywordStuffingDescription</c> returns the RankingFactor related to the site meta description.</summary>
        public static RankingFactor getDescriptionInfoFactor(IWebDriver driver)
        {
            string status = "info";
            string meaningPrefix = "";
            string description = GetDescription(driver);
            if (description == null || description == "")
            {
                description = "None"; 
                status = "bad";
                meaningPrefix = "No description could be found for the given webpage!\n";
            }
            else if (description == driver.Title)
            {
                status = "warning";
                meaningPrefix = "Your description is exactly the same as the title!\n";
            }
            else if (isKeywordStuffingDescription(description))
            {
                status = "warning";
                meaningPrefix = "Your description looks to be keyword stuffed!\n";
            }

            if (driver.FindElements(By.XPath("//meta[@name=\"description\"]")).Count > 1)
            {
                status = "bad";
                meaningPrefix += "More than one description was found on the given webpage.\n";
            }

            RankingFactor descriptionInfoFactor = new RankingFactor("Description")
            {
                value = description,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Content:Description:Info"]
            };
            return descriptionInfoFactor;
        }

        /// <summary>method <c>getDescriptionLengthFactor</c> returns the RankingFactor related to the site meta description length.</summary>
        public static RankingFactor getDescriptionLengthFactor(IWebDriver driver)
        {
            string status;
            string messagePrefix = "";
            string value;
            int possibleTruncateLength = 160;
            int definiteTruncateLength = 360;
            int tooShortLength = 80;
            string description = GetDescription(driver);
            if (description == null || description.Trim().Equals(""))
            {
                status = "bad";
                messagePrefix = "The webpage has no description!\n";
                value = "0 characters; 0 Words";
            }
            else if (description.Length < tooShortLength)
            {
                status = "warning";
                messagePrefix = "Your description may be too short!\n";
                value = description.Length.ToString() + " characters; " + description.Trim().Split(' ').Count() + " Words";
            }
            else if (description.Length > possibleTruncateLength)
            {
                status = "warning";
                messagePrefix = "Your description may be too long!\n";
                value = description.Length.ToString() + " characters; " + description.Trim().Split(' ').Count() + " Words";
            } else if (description.Length > definiteTruncateLength)
            {
                status = "bad";
                messagePrefix = "Your description is way too long!\n";
                value = description.Length.ToString() + " characters; " + description.Trim().Split(' ').Count() + " Words";
            }
            else
            {
                status = "good";
                value = description.Length.ToString() + " characters; " + description.Trim().Split(' ').Count() + " Words";
            }
            RankingFactor titleLengthFactor = new RankingFactor("Length")
            {
                value = value,
                status = status,
                meaning = messagePrefix 
                          + $"Descriptions should be no longer than {definiteTruncateLength} characters long, but could still be cut off for mobile users when above {possibleTruncateLength} characters long." 
                          + Startup.config["Factor_Meanings:Content:Fescription:Info"]
            };
            return titleLengthFactor;
        }

        /// <summary>method <c>GetHeading1InfoFactor</c> returns the RankingFactor related to the first h1 of the site.</summary>
        public static RankingFactor GetHeading1InfoFactor(IWebDriver driver)
        {
            string status = "info";
            string meaningPrefix = "";
            string h1 = null;
            try
            {
                h1 = driver.FindElement(By.XPath("//h1"))?.GetAttribute("textContent"); 
            }
            catch (NoSuchElementException)
            {

            }
            if (h1 == null)
            {
                status = "bad";
                meaningPrefix = "no h1 could be found for the given webpage";
            }
            else if (h1 == "")
            {
                status = "bad";
                meaningPrefix = "An h1 was found, but was presented as empty or with no text in the current viewport.";
            }


            RankingFactor heading1InfoFactor = new RankingFactor("H1")
            {
                value = h1,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Content:Headings:Headings_1_Info"]
            };
            return heading1InfoFactor;
        }

        /// <summary>method <c>GetHeading1CountFactor</c> returns the RankingFactor related to the amount of h1's found on the site.</summary>
        public static RankingFactor GetHeading1CountFactor(IWebDriver driver)
        {
            string status = "good";
            string meaningPrefix = "";
            int h1Count = driver.FindElements(By.XPath("//h1")).Count;
            if (h1Count == 0)
            {
                status = "bad";
                meaningPrefix = "no h1 could be found for the given webpage.";
            }
            else if (h1Count > 1)
            {
                status = "bad";
                meaningPrefix = "More than one h1 was found for the given webpage.";
            }


            RankingFactor heading1CountFactor = new RankingFactor("H1 Count")
            {
                value = h1Count.ToString(),
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Content:Headings:Headings_1_Count"]
            };
            return heading1CountFactor;
        }

        /// <summary>method <c>GetHeadingsFactor</c> returns the MultivaluedRankingFactor related headings on the site.</summary>
        public static MultivaluedRankingFactor GetHeadingsFactor(IWebDriver driver)
        {
            string status = "info";
            HeadingsFactor headings = new HeadingsFactor("Headings", "");
            foreach (IWebElement element in driver.FindElements(By.XPath("//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]")))
            {
                headings.addHeading(element.TagName, element.GetAttribute("textContent"));
            }
            headings.status = status;
            return headings;
        }

        /// <summary>method <c>getBodyContent</c> returns a string of the textcontent in the body of the page.</summary>
        public static string getBodyContent(IWebDriver driver)
        {

            string content = "";
            try
            {
                content = driver.FindElement(By.TagName("body"))?.GetAttribute("textContent");
            }
            catch (NoSuchElementException)
            {

            }
            return content;
        }

        /// <summary>method <c>getContentLengthFactor</c> returns a RankingFactor related to the length of the body content.</summary>
        public static RankingFactor getContentLengthFactor(IWebDriver driver)
        {

            string content = getBodyContent(driver);

            int contentLength = content.Length;
            int wordCount = content.Trim().Split(' ').Count();
            RankingFactor contentLengthFactor = new RankingFactor("Content Length")
            {
                value = contentLength + " characters; " + wordCount + " Words",
                meaning = Startup.config["Factor_Meanings:Content:General:Length"]
            };

            if (contentLength < 500)
            {
                contentLengthFactor.status = "bad";
            }
            else
            {
                contentLengthFactor.status = "good";
            }

            return contentLengthFactor;
        }

        /// <summary>method <c>getTopWordsUsed</c> returns a MultivaluedRankingFactor related to the most used words in a webpage.</summary>
        public static MultivaluedRankingFactor getTopWordsUsed(IWebDriver driver)
        {
            var topWords = driver.FindElement(By.TagName("body")).Text
                            .Replace(',', ' ').Replace('.', ' ').Trim().ToLower() //normalize words
                            .Split(null) // split out the words 
                            .Where(word => word.Length > 4) // filter only for words over 4 characters
                            .GroupBy(word => word) // group by the same words
                            .OrderByDescending(group => group.Count()); // order from greatest to least
            KeywordsFactor keywords = new KeywordsFactor("Keywords", "");
            System.Diagnostics.Debug.WriteLine("getTopWordsUsed: "  + topWords.Count());
            for (int i = 0; i < topWords.Count() && i < 10; i++)
            {
                System.Diagnostics.Debug.WriteLine("getTopWordsUsed: i=" + topWords.ElementAt(i).First() + "  --  " + topWords.ElementAt(i).Count());
                keywords.addKeyword(topWords.ElementAt(i).First(), topWords.ElementAt(i).Count());
            }
            keywords.status = "info";
            return keywords;
        }

        /// <summary>method <c>getCodeToTextRatioFactor</c> returns a RankingFactor related to the ratio between raw HTML and text in the webpage.</summary>
        public static RankingFactor getCodeToTextRatioFactor(IWebDriver driver)
        {
            double codeToTextRatio = ((double)getBodyContent(driver).Length) / ((double)driver.PageSource.Length);
            RankingFactor codeToTextRatioFactor = new RankingFactor("Code to Text Ratio")
            {
                value = string.Format("{0:0.00}%", codeToTextRatio * 100),
                status = "info",
                meaning = Startup.config["Factor_Meanings:Content:General:Code_To_Text_Ratio"]
            };
            return codeToTextRatioFactor;
        }

        /// <summary>method <c>validContentEncoding</c> returns true if the compression content encoding is defined in the HTTP rfc.</summary>
        public static bool validContentEncoding(string contentEncoding)
        {
            string pattern = @"(gzip|compress|deflate|identity|br)(\s*,\s*(gzip|compress|deflate|identity|br))*";
            Match result = Regex.Match(contentEncoding, pattern);
            return result.Success;
        }

        /// <summary>method <c>compressionFactor</c> returns a RankingFactor related to the compression used on the webpage.</summary>
        public static RankingFactor compressionFactor(ConcurrentDictionary<Uri, ResponseData> responseData, string url)
        {
            string encoding = responseData[new Uri(url)].ContentEncoding();
            string value = "";
            string status = "bad";
            if(encoding == null || encoding.Trim().Equals(""))
            {
                value = "None";
            } else if (!validContentEncoding(encoding))
            {
                value = encoding;
            }
            else
            {
                value = encoding;
                status = "good";
            }
            RankingFactor compressionFactor = new RankingFactor("Content-Encoding")
            {
                value = value,
                status = status,
                meaning = Startup.config["Factor_Meanings:Content:General:Compression"]
            };
            return compressionFactor;
        }

        /// <summary>method <c>GetGoogleSnippet</c> returns a Dictionary corresponding to the information in a Google search snippet.</summary>
        public static Dictionary<string, string> GetGoogleSnippet(string url, IWebDriver driver)
        {
            Dictionary<string, string> googleSnippet = new Dictionary<string, string> { };
            var description = GetDescription(driver);
            if(description != null)
            {
                description = Truncate(description, 160);
            }
            var title = driver.Title;
            if (title != null)
            {
                title = Truncate(title, 60);
            }
            googleSnippet["description"] = description;
            googleSnippet["title"] = title;
            googleSnippet["url"] = url;
            return googleSnippet;
        }
    }
}
