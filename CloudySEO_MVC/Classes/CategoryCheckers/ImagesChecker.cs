﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Concurrent;

namespace CloudySEO_MVC.Classes.CategoryCheckers
{
    public class ImagesChecker : CategoryChecker
    {

        /// <summary>method <c>ImagesCheck</c> returns back the RankingCategory data for all webpage images.</summary>
        public static RankingCategory ImagesCheck(IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData, string url)
        {
            // favicon
            //          check if //xpath[@rel='shortcut icon']
            //                    Check if loaded properly
            //          else check if /favicon.ico is served by the server
            //                    Check if loaded properly
            // image count


            RankingCategory images = new RankingCategory("Images");
            // check that each image has Alt attribute, Title attribute, (size)

            RankingSubcategory imageList = new RankingSubcategory("Images Displayed");
            imageList.factors.Add(faviconFactor(driver, responseData, url));
            imageList.multivaluedFactors.Add(getImagesFactor(driver, responseData));
            images.subcategories.Add(imageList);

            return images;
        }

        /// <summary>method <c>faviconFactor</c> returns back the RankingFactor data for the webpage's favicon.</summary>
        public static RankingFactor faviconFactor(IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData, string url)
        {
            string value = "favicon not found";
            string status = "bad";
            string meaning = "The favicon for the webpage was not found.";
            int statusCode = 999;

            IWebElement faviconElement = null;
            string elementUrl = null;

            // try and use standard, .ico icon
            try
            {
                faviconElement = driver.FindElement(By.XPath("//link[@rel='shortcut icon']"));
                elementUrl = faviconElement.GetAttribute("src");
                System.Diagnostics.Debug.WriteLine("Set favicon url from src to: " + elementUrl);
            } catch(Exception e) {
                System.Diagnostics.Debug.WriteLine("Exception in faviconFactor(): " + e.Message);
            }

            // if .ico icon was not set, then try and use the alternative format for the icon
            try
            {
                if (elementUrl == null || elementUrl.Equals(""))
                {
                    faviconElement = driver.FindElement(By.XPath("//link[@rel='icon']"));
                    elementUrl = faviconElement.GetAttribute("href");
                    System.Diagnostics.Debug.WriteLine("Set favicon url from href to: " + elementUrl);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Favicon url already set from src to: " + elementUrl);
                }
            } catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception in faviconFactor(): " + e.Message);
            }

            // if the .ico or alternative icon were found and the elementUrl set properly, then check if that loads properly
            if (elementUrl != null && !elementUrl.Equals(""))
            {
                statusCode = checkFileStatusCode(elementUrl);
                if (statusCode == 200)
                {
                    value = "favicon found";
                    status = "good";
                    meaning = $"{elementUrl} was found";
                }
            }
            if (statusCode != 200) // if the icon could not be checked by the previous two checks, then try statically at the /favicon.ico location
            {
                Uri uri = new UriBuilder(url).Uri;
                string favicon = uri.GetLeftPart(System.UriPartial.Authority) + "/favicon.ico";
                if (checkIfFileExists(favicon))
                {
                    status = "good";
                    value = "favicon exists";
                    meaning = $"{favicon} was found.";
                }
            }
            RankingFactor faviconFactor = new RankingFactor("Favicon")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Images:UX_Based:Favicon"]
            };
            return faviconFactor;
        }
        
        /// <summary>method <c>getImagesFactor</c> returns back the MultivaluedRankingFactor data for all embedded images in the webpage.</summary>
        public static MultivaluedRankingFactor getImagesFactor(IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData)
        {
            string status = "info";
            ImagesFactor images = new ImagesFactor("Images", "");
            foreach (IWebElement element in driver.FindElements(By.CssSelector("img")))
            {
                string url = element.GetAttribute("src");
                int statusCode = 999;
                try
                {
                    statusCode = responseData[new Uri(url)].statusCode;
                }
                catch (Exception ex)
                {

                }
                images.addImage(element.GetAttribute("alt"), element.GetAttribute("title"), element.GetAttribute("src"), statusCode);
            }
            images.status = status;
            return images;
        }
    }
}
