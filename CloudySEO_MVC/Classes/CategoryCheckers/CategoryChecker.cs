﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;

namespace CloudySEO_MVC.Classes.CategoryCheckers
{
    public class CategoryChecker
    {
        /// <summary>method <c>GetMetaTagInformation</c> retrieves metatag content.</summary>
        public static String GetMetaTagInformation(string metatagName, IWebDriver driver)
        {
            String tagContent;
            try
            {
                IWebElement element = driver.FindElement(By.XPath($"//meta[@name=\"{metatagName}\"]"));
                tagContent = element.GetAttribute("content");
            }
            catch (NoSuchElementException)
            {
                tagContent = null; // probably unnecessary..
            }
            return tagContent;
        }

        /// <summary>method <c>GetSubDomain</c> returns the subdomain of a url. Based on https://madskristensen.net/blog/retrieve-the-subdomain-from-a-url-in-c/ </summary>
        protected static string GetSubDomain(Uri url)
        {
            if (url.HostNameType == UriHostNameType.Dns)
            {
                string host = url.Host;
                if (host.Split('.').Length > 2)
                {
                    int lastIndex = host.LastIndexOf(".");
                    int index = host.LastIndexOf(".", lastIndex - 1);
                    return host.Substring(0, index);
                }
            }
            return null;
        }

        /// <summary>method <c>checkFileStatusCode</c> returns the resulting status code when HTTP requesting the given url. Based on https://dotnetthoughts.net/2009/10/14/how-to-check-remote-file-exists-using-c/ </summary>
        protected static int checkFileStatusCode(string url)
        {
            int statusCode = 999;
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.UserAgent = "CloudySEO Tool";
                request.Timeout = 10000;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                System.Diagnostics.Debug.WriteLine("checkIfFileExists() status code: " + response.StatusCode.ToString() + "; for uri:  " + url);
                statusCode = (int)response.StatusCode;
                response.Close();
            }
            catch (WebException we)
            {
                using (WebResponse response = we.Response)
                {
                    System.Diagnostics.Debug.WriteLine(we.Message);
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    if (httpResponse != null)
                    {
                        statusCode = (int)httpResponse.StatusCode;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("exception in checkIfFileExists(): " + ex.Message);
                // assume that if exception happens, it doesn't
            }
            return statusCode;
        }

        /// <summary>method <c>checkIfFileExists</c> returns true if the site returns a 200 OK response.</summary>
        protected static bool checkIfFileExists(string url)
        {
            return checkFileStatusCode(url) == 200;
        }


        /// <summary>method <c>Truncate</c> truncates the string up to the maxLength.</summary>
        protected static string Truncate(string value, int maxLength)
        {
            return value.Length <= maxLength ? value : value.Substring(0, maxLength-3) + "...";
        }
    }
}
