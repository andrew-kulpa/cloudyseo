﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Threading;
using OpenQA.Selenium;

namespace CloudySEO_MVC.Classes.CategoryCheckers
{
    public class LinksChecker : CategoryChecker
    {

        /// <summary>method <c>LinksCheck</c> returns the RankingCategory data related to the in-page links.</summary>
        public static RankingCategory LinksCheck(IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData, string url)
        {
            RankingCategory links = new RankingCategory("Links");


            RankingSubcategory elementsChecks = new RankingSubcategory("Elements");
            elementsChecks.factors.Add(getScriptsFactor(driver, responseData));
            elementsChecks.factors.Add(getCSSFactor(driver, responseData));
            links.subcategories.Add(elementsChecks);

            // check external links
            //      link, anchor text, status code
            RankingSubcategory externalLinks = new RankingSubcategory("External Links");
            externalLinks.multivaluedFactors.Add(getExternalLinksFactor(url, driver));
            links.subcategories.Add(externalLinks);

            // check internal links
            //      link, anchor text, status code
            RankingSubcategory internalLinks = new RankingSubcategory("Internal Links");
            internalLinks.multivaluedFactors.Add(getInternalLinksFactor(url, driver));
            links.subcategories.Add(internalLinks);

            return links;
        }

        /// <summary>method <c>getScriptsFactor</c> returns the RankingFactor for in-page scripts, accumulating warnings and errors based on status codes.</summary>
        public static RankingFactor getScriptsFactor(IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData)
        {
            string status = "info";
            int warnings = 0;
            int errors = 0;
            foreach (IWebElement element in driver.FindElements(By.TagName("script")))
            {
                string url = element.GetAttribute("src");
                if (url == null || url.Equals(""))
                {
                    continue;
                }

                int statusCode = 999;
                try
                {
                    statusCode = responseData[new Uri(url)].statusCode;
                    if (statusCode > 299 && statusCode < 400)
                    {
                        warnings++;
                    }
                    else if (statusCode > 399)
                    {
                        errors++;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (errors > 0)
            {
                status = "bad";
            }
            else if (warnings > 0)
            {
                status = "warning";
            }
            RankingFactor scriptsFactor = new RankingFactor("Scripts")
            {
                value = $"{errors} client or server error status codes; {warnings} redirection status codes",
                status = status,
                meaning = Startup.config["Factor_Meanings:Links:Embedded_Elements:Scripts_Loaded_OK"]
            };
            return scriptsFactor;
        }

        /// <summary>method <c>getCSSFactor</c> returns the RankingFactor for in-page stylesheets, accumulating warnings and errors based on status codes.</summary>
        public static RankingFactor getCSSFactor(IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData)
        {
            string status = "info";
            int warnings = 0;
            int errors = 0;
            foreach (IWebElement element in driver.FindElements(By.TagName("css")))
            {
                string url = element.GetAttribute("src");
                if (url == null || url.Equals(""))
                {
                    continue;
                }

                int statusCode = 999;
                try
                {
                    statusCode = responseData[new Uri(url)].statusCode;
                    if (statusCode > 299 && statusCode < 400)
                    {
                        warnings++;
                    }
                    else if (statusCode > 399)
                    {
                        errors++;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (errors > 0)
            {
                status = "bad";
            }
            else if (warnings > 0)
            {
                status = "warning";
            }
            RankingFactor scriptsFactor = new RankingFactor("Stylesheets")
            {
                value = $"{errors} client or server error status codes; {warnings} redirection status codes",
                status = status,
                meaning = Startup.config["Factor_Meanings:Links:Embedded_Elements:CSS_Loaded_OK"]
            };
            return scriptsFactor;
        }


        /// <summary>method <c>getInternalLinksFactor</c> returns the MultivaluedRankingFactor data for all links with the same base host.</summary>
        public static MultivaluedRankingFactor getInternalLinksFactor(string url, IWebDriver driver)
        {
            string status = "info";
            LinksFactor internalLinks = new LinksFactor("Internal Links", "");
            Uri currentURI = new UriBuilder(url).Uri;
            List<Thread> threads = new List<Thread>();
            ConcurrentDictionary<Uri, int> linkStatusCodeCache = new ConcurrentDictionary<Uri, int> { };
            foreach (IWebElement element in driver.FindElements(By.XPath("//*[self::a or self::link]")))
            {
                Thread linkThread = new Thread(() => {
                    try
                    {
                        string elementURL = element.GetAttribute("href");
                        Uri elementURI = new UriBuilder(elementURL).Uri;
                        System.Diagnostics.Debug.WriteLine("Checking link in getInternalLinksFactor(): " + elementURL);
                        if (!elementURL.StartsWith("tel:") && !elementURL.StartsWith("mailto:") && elementURI.Host == currentURI.Host && elementURI != currentURI) // is internal link
                        {
                            string elementText = null;
                            if (element.GetAttribute("textContent") != null)
                            {
                                elementText = element.GetAttribute("textContent").Substring(0, Math.Min(30, element.GetAttribute("textContent").Length));
                            }
                            if (!linkStatusCodeCache.ContainsKey(elementURI))
                                linkStatusCodeCache.GetOrAdd(elementURI, checkFileStatusCode(elementURL));
                            internalLinks.addLink(element.TagName, elementURL, elementText, linkStatusCodeCache[elementURI]);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine("exception in getInternalLinksFactor(): " + ex.Message);
                    }
                });
                threads.Add(linkThread);
                linkThread.Start();
            }
            foreach (Thread thread in threads)
            {
                thread.Join();
            }
            internalLinks.status = status;
            return internalLinks;
        }

        /// <summary>method <c>getExternalLinksFactor</c> returns the MultivaluedRankingFactor data for all links without the same base host.</summary>
        public static MultivaluedRankingFactor getExternalLinksFactor(string url, IWebDriver driver)
        {
            string status = "info";
            LinksFactor externalLinks = new LinksFactor("External Links", "");
            Uri currentURI = new UriBuilder(url).Uri;
            List<Thread> threads = new List<Thread>();
            ConcurrentDictionary<Uri, int> linkStatusCodeCache = new ConcurrentDictionary<Uri, int> { };
            foreach (IWebElement element in driver.FindElements(By.XPath("//*[self::a or self::link]")))
            {

                Thread linkThread = new Thread(() => {
                    try
                    {
                        string elementURL = element.GetAttribute("href");
                        Uri elementURI = new UriBuilder(elementURL).Uri;
                        System.Diagnostics.Debug.WriteLine("Checking link in getExternalLinksFactor(): " + elementURL);
                        if (!elementURL.StartsWith("tel:") && !elementURL.StartsWith("mailto:") && new UriBuilder(elementURL).Host != currentURI.Host) // is external link
                        {
                            string elementText = null;
                            if (element.GetAttribute("textContent") != null)
                            {
                                elementText = element.GetAttribute("textContent").Substring(0, Math.Min(30, element.GetAttribute("textContent").Length));
                            }
                            if (!linkStatusCodeCache.ContainsKey(elementURI))
                                linkStatusCodeCache.GetOrAdd(elementURI, checkFileStatusCode(elementURL));
                            externalLinks.addLink(element.TagName, elementURL, elementText, linkStatusCodeCache[elementURI]);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine("exception in getExternalLinksFactor(): " + ex.Message);
                    }
                });
                threads.Add(linkThread);
                linkThread.Start();
            }
            foreach (Thread thread in threads)
            {
                thread.Join();
            }
            externalLinks.status = status;
            return externalLinks;
        }

    }
}
