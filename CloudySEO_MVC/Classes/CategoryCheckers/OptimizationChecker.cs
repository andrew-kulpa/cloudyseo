﻿using System;
using OpenQA.Selenium;
using System.Net;
using System.IO;
using RobotsTxt;
using System.Text;
using System.Collections.Concurrent;

namespace CloudySEO_MVC.Classes.CategoryCheckers
{
    public class OptimizationChecker : CategoryChecker
    {

        /// <summary>method <c>optimizationCategory</c> returns the RankingCategory data for all optimization factors.</summary>
        public static RankingCategory optimizationCategory(string initialSanitizedURL, string url, IWebDriver driver, ConcurrentDictionary<Uri, ResponseData> responseData)
        {
            RankingCategory optimization = new RankingCategory("Index Optimization");
            
            RankingSubcategory indexChecks = new RankingSubcategory("Indexation");
            indexChecks.factors.Add(getRobotsTXTFactor(url));
            indexChecks.factors.Add(getSitemapFactor(url)); // sitemap.xml
            indexChecks.factors.Add(allowedByRobotsTXTFactor(url)); // robots.txt
            indexChecks.factors.Add(allowedByRobotsTagFactor(driver)); // robots meta tag
            indexChecks.factors.Add(getKeywordsInfoFactor(driver)); // keywords meta tag
            indexChecks.factors.Add(underscoreInPathFactor(url)); // underscores in path
            indexChecks.factors.Add(canonicalRedirectFactor(driver, url, initialSanitizedURL));
            optimization.subcategories.Add(indexChecks);

            RankingSubcategory crawlabilityChecks = new RankingSubcategory("Crawlability");
            crawlabilityChecks.factors.Add(hasIFramesFactor(driver)); //check for iframes
            crawlabilityChecks.factors.Add(getFlashFactor(driver)); // check for flash content
            optimization.subcategories.Add(crawlabilityChecks);

            RankingSubcategory MobileUserExperienceChecks = new RankingSubcategory("Mobile User Experience / Speed");
            MobileUserExperienceChecks.factors.Add(fitsScreenwidth(driver));// fits to viewport
            MobileUserExperienceChecks.factors.Add(getViewportInfoFactor(driver)); // print out what the viewport is
            MobileUserExperienceChecks.factors.Add(hasCustom404Pages(url)); // check if a custom 404 exists
            MobileUserExperienceChecks.factors.Add(httpsRedirectFactor(url, initialSanitizedURL)); // check https-<->http redirects
            optimization.subcategories.Add(MobileUserExperienceChecks);

            RankingSubcategory Headers = new RankingSubcategory("Headers");
            Headers.multivaluedFactors.Add(GetHeadersFactor(responseData, url));
            optimization.subcategories.Add(Headers);

            return optimization;
        }

        /// <summary>method <c>GetHeadersFactor</c> returns the MultivaluedRankingFactor data for all response headers.</summary>
        public static MultivaluedRankingFactor GetHeadersFactor(ConcurrentDictionary<Uri, ResponseData> responseData, string url)
        {
            string status = "info";
            HeadersFactor headers = new HeadersFactor("Headers", "");
            foreach(var header in responseData[new Uri(url)].getHeaders())
            {
                headers.addHeader(header.Key,header.Value);
            }
            headers.status = status;
            return headers;
        }
        
        /// <summary>method <c>getViewport</c> returns a string of the viewport meta tag content.</summary>
        public static String getViewport(IWebDriver driver)
        {
            return GetMetaTagInformation("viewport", driver);
        }

        /// <summary>method <c>getViewportInfoFactor</c> returns a RankingFactor setting the status to warning if the viewport isnt set properly.</summary>
        public static RankingFactor getViewportInfoFactor(IWebDriver driver)
        {
            string status = "info";
            string meaningPrefix = "";
            string viewport = getViewport(driver);
            if (viewport != null && viewport != "" && viewport == driver.Title)
            {
                status = "warning";
                meaningPrefix = "No viewport is set for the webpage. This means that your webpage may be less optimized for mobile devices.\n";
            }
            RankingFactor keywordsInfoFactor = new RankingFactor("Meta Viewport")
            {
                value = viewport,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Optimization:Mobile:Viewport_Info"]
        };
            return keywordsInfoFactor;
        }

        /// <summary>method <c>getFlashFactor</c> returns a RankingFactor setting the status to bad if and embed or object tags have .swf based content.</summary>
        public static RankingFactor getFlashFactor(IWebDriver driver)
        {
            string status = "good";
            int flashObjects = 0;
            foreach (IWebElement element in driver.FindElements(By.TagName("embed")))
            {
                string url = element.GetAttribute("src");
                if (url != null && url.Contains(".swf"))
                {
                    flashObjects++;
                }
            }
            foreach (IWebElement element in driver.FindElements(By.TagName("object")))
            {
                string url = element.GetAttribute("data");
                if (url != null && url.Contains(".swf"))
                {
                    flashObjects++;
                }
            }

            if (flashObjects > 0)
            {
                status = "bad";
            }
            RankingFactor flashFactor = new RankingFactor("Flash Content")
            {
                value = $"{flashObjects} embedded Flash objects",
                status = status,
                meaning = Startup.config["Factor_Meanings:Optimization:Crawlability:Has_Flash_Content"]
            };
            return flashFactor;
        }

        /// <summary>method <c>fitsScreenwidth</c> returns a RankingFactor setting the status to bad if the content does not fit the screen width perfectly.</summary>
        public static RankingFactor fitsScreenwidth(IWebDriver driver)
        {
            var js = (IJavaScriptExecutor)driver;
            bool isOverflowing = (bool)js.ExecuteScript("return document.querySelector('body').clientWidth <= window.innerWidth");

            string status;
            string value;
            string meaning;
            if (isOverflowing)
            {
                status = "good";
                value = "The content fits the screen width";
                meaning = $"The content that was presented does fits the width of the screen described in the viewport of the webpage.";
            }
            else
            {
                status = "bad";
                value = "The content does not fit the screen width presented.";
                meaning = $"The content that was presented does not fit the width of the screen described in the viewport of the webpage.";

            }
            RankingFactor sitemapFactor = new RankingFactor("Fit Width")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Optimization:Mobile:Fits_Viewport"]
            };
            return sitemapFactor;
        }

        /// <summary>method <c>hasIFramesFactor</c> returns a RankingFactor setting the status to bad if any iframes were found in the page.</summary>
        public static RankingFactor hasIFramesFactor(IWebDriver driver)
        {
            string status = "good";
            string meaningPrefix = "";
            int iframes = driver.FindElements(By.XPath("//iframe")).Count;
            if (iframes > 0)
            {
                status = "bad";
                meaningPrefix = "IFrames were found in the webpage.";
            }
            
            RankingFactor hasIFramesFactor = new RankingFactor("iframe count")
            {
                value = iframes.ToString() + " Frames",
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Optimization:Crawlability:Has_IFrames"]
            };
            return hasIFramesFactor;
        }

        /// <summary>method <c>GetRobotsTagInformation</c> returns a string with robots meta tag content.</summary>
        public static string GetRobotsTagInformation(IWebDriver driver)
        {
            return GetMetaTagInformation("robots", driver);
        }

        /// <summary>method <c>allowedByRobotsTagFactor</c> returns a RankingFactor with a bad status if the robots meta tag prohibits crawling.</summary>
        public static RankingFactor allowedByRobotsTagFactor(IWebDriver driver)
        {
            string status = "good";
            string meaningPrefix = "";
            string robotsValue = GetRobotsTagInformation(driver);

            System.Diagnostics.Debug.WriteLine("allowedByRobotsTagFactor() status code: " + robotsValue);
            if (robotsValue != null && robotsValue.Contains("noindex"))
            {
                status = "bad";
                meaningPrefix = "The robots meta tag content includes noindex. For this reason, search engines will not index the webpage.";
            }
            else if (robotsValue == null)
            {
                robotsValue = "Robots meta tag could not be found";
                status = "good";
                meaningPrefix = "The robots meta tag content could not be found. The web page is implicitly indexable.";
            }
            else
            {
                status = "good";
                meaningPrefix = "The robots meta tag content was found, but did not include the noindex directive. The web page is implicitly indexable.";
            }


            RankingFactor allowedByRobotsTagFactor = new RankingFactor("Robots Meta Tag")
            {
                value = robotsValue,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Optimization:Indexation:Allowed_By_Robots_Meta_Tag"]
            };
            return allowedByRobotsTagFactor;
        }

        /// <summary>method <c>check404Page</c> returns a 2 element int array {status_code, is_properly_custom?}.</summary>
        protected static int[] check404Page(string url)
        {
            int maxRedirects = 30;
            int currentRedirects = 0;
            int[] statuses = new int[2] { 999, 0 };
            while (!string.IsNullOrWhiteSpace(url) && currentRedirects < maxRedirects)
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine($"check404Page url check: {url}");
                    HttpWebRequest request = HttpWebRequest.CreateHttp(url);
                    request.UserAgent = "CloudySEO Tool";
                    request.AllowAutoRedirect = false;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        string prevUrl = url;
                        url = response.GetResponseHeader("Location");
                        statuses[0] = (int)response.StatusCode;
			            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), Encoding.GetEncoding(response.CharacterSet.Replace("\"",""))))
                        {
                            string responseText = reader.ReadToEnd().ToLower();
                            bool notCustomEnough = responseText.Contains("http error 404") && responseText.Contains("file not found");
                            statuses[1] = notCustomEnough ? 0 : 1;
                        }
			            System.Diagnostics.Debug.WriteLine($"status code for url {prevUrl} was {statuses[0]} with custom enough = {statuses[1]}");
                        response.Close();
                    }
                }
                catch (WebException we)
                {
                    using (WebResponse response = we.Response)
                    {
                        System.Diagnostics.Debug.WriteLine(we.Message);
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        if (httpResponse != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Error code: {0} {1}", (int)httpResponse.StatusCode, httpResponse.StatusCode);

                            statuses[0] = (int)httpResponse.StatusCode;
                            using (var reader = new System.IO.StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(httpResponse.CharacterSet.Replace("\"",""))))
                            {
                                string responseText = reader.ReadToEnd().ToLower();
                                bool notCustomEnough = responseText.Contains("http error 404") && responseText.Contains("file not found");
                                statuses[1] = notCustomEnough ? 0 : 1;
                            }
                            url = httpResponse.GetResponseHeader("Location");
                            httpResponse.Close();
                        }

                    }
                } 
		catch(Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Error message: {0} \n\nStacktrace:\n{1}", ex.Message, ex.StackTrace);
                    return new int[2] { 999, 0 };
                }
                currentRedirects += 1;
            }
            return statuses;
        }
        
        /// <summary>method <c>hasCustom404Pages</c> returns a RankingFactor with a status set to warning or bad based on the 404 page's response.</summary>
        public static RankingFactor hasCustom404Pages(string url)
        {
            string status;
            string value;
            System.Uri uri = new UriBuilder(url).Uri;
            string bogusURI = uri.GetLeftPart(System.UriPartial.Authority) + "/CloudySEO/404/hopefully/bogus/url/test";
            string meaning;
            int[] statuses = check404Page(bogusURI); // { StatusCode, isCustomEnough}
            int statuscode = statuses[0];
            if (statuscode == 404)
            {
                status = "good";
                if(statuses[1] == 1)
                {
                    value = "Custom 404 webpage exists";
                    meaning = $"A custom 404 webpage was found. ";
                }
                else
                {
                    value = "Generic 404 webpage found";
                    meaning = $"A 404 webpage was found, but it appears to be generic and not custom enough for a general user. ";
                }
            }
            else if(statuscode > 399 && statuscode < 500) // 400 response, but not 404
            {
                status = "warning";
                value = "Custom 404 webpage could not be found";
                meaning = $"A {statuscode} response was received, so the expected 404 response was not received. This means a webcrawler may not recognize the page is not a valid resource and instead another error occurred in its request. ";

            }
            else if (statuscode > 499 && statuscode < 600) // presumably a server error
            {
                status = "bad";
                value = "Custom 404 webpage could not be found";
                meaning = $"A {statuscode} response was received, so the expected 404 response was not received. This means a webcrawler may not recognize the page is not a valid resource and instead a server error had occurred. ";
            }
            else if(statuscode < 400 && statuscode > 299) // redirected
            {
                status = "warning";
                value = "Custom 404 webpage could not be found";
                meaning = $"A {statuscode} response was received, so the expected 404 response was not received. This is likely due to a redirect loop. ";
            }
            else if(statuscode < 300 && statuscode > 199)
            {
                status = "warning";
                value = "Custom 404 webpage could not be found";
                meaning = $"A {statuscode} response was received, but the 404 response was not received as expected. This means a webcrawler may not recognize the page is not a valid resource.";
            } else if (statuscode == 999 )
            {
                status = "bad";
                value = "Custom 404 webpage could not be found";
                meaning = $"The web server failed to return a valid response for a missing object. ";
            }
            else
            {
                status = "bad";
                value = "Custom 404 webpage could not be found";
                meaning = $"A {statuscode} response was received, so the expected 404 response was not received. A webcrawler may not know what to do with such a status code. ";
            }
            RankingFactor custom404factor = new RankingFactor("Custom 404")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Optimization:Mobile:Custom_404"]
            };
            return custom404factor;
        }

        /// <summary>method <c>getRobotsTXTFactor</c> returns a RankingFactor for robots.txt meta tag content and warning if it doesn't exist or is empty.</summary>
        public static RankingFactor getRobotsTXTFactor(string url)
        {
            string status;
            string value;
            System.Uri uri = new UriBuilder(url).Uri;
            string robotsTXTURI = uri.GetLeftPart(System.UriPartial.Authority) + "/robots.txt";
            string meaning;
            if (checkIfFileExists(robotsTXTURI))
            {
                status = "good";
                value = "robots.txt exists";
                meaning = $"{robotsTXTURI} was found. This means that the website provides limits on what can and cannot be indexed.";
            }
            else
            {
                status = "warning";
                value = "robots.txt could not be found";
                meaning = $"{robotsTXTURI} was not found. This means that the website does not provide limits on what can and cannot be indexed.";

            }
            RankingFactor robotsTXTFactor = new RankingFactor("Robots.txt")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Optimization:Indexation:Robots_TXT_Exists"]
            };
            return robotsTXTFactor;
        }

        /// <summary>method <c>allowedByRobotsTXTFactor</c> returns a RankingFactor for robots.txt meta tag crawling allowance.</summary>
        public static RankingFactor allowedByRobotsTXTFactor(string url)
        {
            string status;
            string value;
            System.Uri uri = new UriBuilder(url).Uri;
            string robotsTXTURI = uri.GetLeftPart(System.UriPartial.Authority) + "/robots.txt";
            string meaning;
            string content = null;
            try // based on https://stackoverflow.com/questions/3273205/read-text-from-response for getting a datastream
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.UserAgent = "CloudySEO Tool";
                request.Method = "GET";
                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    HttpStatusCode statusCode = ((HttpWebResponse)response).StatusCode;
                    content = reader.ReadToEnd();
                }
            } catch( Exception ex)
            {

                System.Diagnostics.Debug.WriteLine($"failed to retrieve robots.txt at {url} due to exception: " + ex.Message + "\n" + ex.StackTrace);
            }

            if (content != "" && content != null)
            {
                Robots robots = Robots.Load(content);
                bool canIGoThere = robots.IsPathAllowed("GoogleBot", new UriBuilder(url).Path);
                if (canIGoThere)
                {
                    status = "good";
                    value = "Allowed by robots.txt";
                    meaning = $"{robotsTXTURI} was found, and it allowed the crawling of this webpage.";
                }
                else
                {
                    status = "bad";
                    value = "disallowed by robots.txt";
                    meaning = $"{robotsTXTURI} was found, but it prohibits crawling of this webpage.";
                }
            }
            else
            {
                status = "warning";
                value = "robots.txt could not be found or was empty";
                meaning = $"{robotsTXTURI} was not found or was empty. This means that the website does not provide limits on what can and cannot be indexed.";

            }
            RankingFactor robotsTXTFactor = new RankingFactor("Crawlability by Robots.txt")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Optimization:Indexation:Allowed_By_Robots_TXT"]
            };
            return robotsTXTFactor;
        }

        /// <summary>method <c>underscoreInPathFactor</c> returns a RankingFactor with a bad status only if an underscore is within the final resolved url.</summary>
        public static RankingFactor underscoreInPathFactor(string url)
        {
            string status;
            string value;
            System.Uri uri = new UriBuilder(url).Uri;
            bool hasUnderscore = uri.AbsolutePath.Contains("_");
            string meaning;
            if (hasUnderscore)
            {
                status = "bad";
                value = "Underscore found in path";
                meaning = "An underscore was found in the path of the url.\n";
            }
            else
            {
                status = "good";
                value = "No underscores found in path";
                meaning = "An underscore was not found in the path of the url.\n";

            }
            RankingFactor underscoreInPathFactor = new RankingFactor("Underscore in Path")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Optimization:Indexation:Underscore_In_Path"]
            };
            return underscoreInPathFactor;
        }

        /// <summary>method <c>getSitemapFactor</c> returns a RankingFactor with a bad statusif a sitemap cannot be found.</summary>
        public static RankingFactor getSitemapFactor(string url)
        {
            string status;
            string value;
            System.Uri uri = new UriBuilder(url).Uri;
            string sitemapURI = uri.GetLeftPart(System.UriPartial.Authority) + "/sitemap.xml";
            string meaning;
            if (checkIfFileExists(sitemapURI))
            {
                status = "good";
                value = "sitemap.xml exists";
                meaning = $"{sitemapURI} was found. These are intended for search engines to help search engines and spiders navigate through websites.";
            }
            else
            {
                status = "bad";
                value = "sitemap.xml could not be found";
                meaning = $"{sitemapURI} was not found. These are intended for search engines to help search engines and spiders navigate through websites.";

            }
            RankingFactor sitemapFactor = new RankingFactor("Sitemap.xml")
            {
                value = value,
                status = status,
                meaning = meaning + Startup.config["Factor_Meanings:Optimization:Indexation:Sitemap_Exists"]
            };
            return sitemapFactor;
        }

        /// <summary>method <c>getKeywords</c> returns a string containing the keywords meta tag content.</summary>
        public static String GetKeywords(IWebDriver driver)
        {
            return GetMetaTagInformation("keywords", driver);
        }

        /// <summary>method <c>getKeywordsInfoFactor</c> returns a RankingFactor warning if keywords are the same as the title or setting a bad status if no keywords tag found.</summary>
        public static RankingFactor getKeywordsInfoFactor(IWebDriver driver)
        {
            string status = "info";
            string meaningPrefix = "";
            string keywords = GetKeywords(driver);

            System.Diagnostics.Debug.WriteLine("getKeywordsInfoFactor() status code: " + keywords);
            if (keywords != null && keywords != "" && keywords == driver.Title)
            {
                status = "warning";
                meaningPrefix = "Keywords meta tag is the same as the title. ";
            }
            else if (keywords == null)
            {
                status = "bad";
                keywords = "Keywords meta tag could not be found";
                meaningPrefix = "No keywords meta tag could be found. ";
            }
            RankingFactor keywordsInfoFactor = new RankingFactor("Meta Keywords")
            {
                value = keywords,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Optimization:Indexation:Keywords_Meta_Tag_Information"]
            };
            return keywordsInfoFactor;
        }
        
        /// <summary>method <c>SeenAsTwoSites</c> returns true if the two strings are different. Really a helper function to make that conditional more clear for https redirect checking.</summary>
        /// <remarks>
        /// When the initial and eventual resolved uri's are the same, all this with the opposite uri scheme
        /// returns true if the current two schemes of the uri's are equivalent, in other words the response does not redirect to the same uri found in the scraping
        /// 
        /// 4 cases:
        ///      http --> https
        ///      http --> http
        ///      https --> http
        ///      https --> https
        ///</remarks>
        protected static bool SeenAsTwoSites(string scheme1, string scheme2)
        {
            bool seenAsTwoSites = true;
            if(scheme1 != scheme2)
            {
                seenAsTwoSites = false;
            }
            return seenAsTwoSites;
        }

        /// <summary>method <c>checkHTTPSRedirect</c> returns true if the http-->https redirect happens improperly, otherwise false if it explicitly redirects correctly.</summary>
        protected static bool checkHTTPSRedirect(string url)
        {
            int maxRedirects = 30;
            int currentRedirects = 0;
            bool seenAsTwoSites = true;
            string initialUrl = url;
            while (!string.IsNullOrWhiteSpace(url) && currentRedirects < maxRedirects)
            {

                HttpWebRequest request = HttpWebRequest.CreateHttp(url);
                request.UserAgent = "CloudySEO Tool";
                request.AllowAutoRedirect = false;
                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        url = response.GetResponseHeader("Location");
                        if (url == null || url.Trim().Equals(""))
                        {
                            break;
                        }
                        System.Diagnostics.Debug.WriteLine("checking redirection: {0} {1}", initialUrl, url);
                        seenAsTwoSites = SeenAsTwoSites((new Uri(initialUrl)).Scheme, (new Uri(url)).Scheme);
                        response.Close();
                    }
                }
                catch (WebException we)
                {
                    using (WebResponse response = we.Response)
                    {
                        System.Diagnostics.Debug.WriteLine("exception in checkHTTPSRedirect(): " + we.Message + " for url:" + url);
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        if (httpResponse != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Error code: {0} {1}", (int)httpResponse.StatusCode, httpResponse.StatusCode);
                            url = httpResponse.GetResponseHeader("Location");
                            System.Diagnostics.Debug.WriteLine("checking redirection: {0} {1}", initialUrl, url);
                            seenAsTwoSites = SeenAsTwoSites((new Uri(initialUrl)).Scheme, (new Uri(url)).Scheme);
                            httpResponse.Close();
                        }

                    }
                }
                currentRedirects += 1;
            }
            return seenAsTwoSites;
        }

        /// <summary>method <c>httpsRedirectFactor</c> returns a RankingFactor warning if the HTTPS redirect is improper and bad if there is no redirect at all.</summary>
        public static RankingFactor httpsRedirectFactor(string url, string initialSanitizedURL)
        {
            string status = "good";
            string meaningPrefix = "";
            string value = "";

            Uri uri = new UriBuilder(initialSanitizedURL).Uri;
            Uri resolvedUri = new UriBuilder(url).Uri;
            if (uri.Scheme != resolvedUri.Scheme)
            {
                if (uri.Scheme.Equals("https")) // https redirects to http
                {
                    status = "warning";
                    meaningPrefix = $"The URL entered eventually redirects to one using HTTP.\n"; // assume if canonical tag set, is fine
                    value = "HTTPS redirects to HTTP";
                }
                else //  http redirects to https
                {
                    meaningPrefix = "The URL entered eventually redirects to one using HTTPS.\n"; // assume if canonical tag set, is fine
                    value = "HTTP redirects to HTTPS";
                }
            }
            else // have to check since http <--> https redirect is unknown now
            {
                // CHECK
                string subdomain = GetSubDomain(uri);
                UriBuilder initialUriBuilder = new UriBuilder(initialSanitizedURL);
                initialUriBuilder.Scheme = initialUriBuilder.Scheme == "http" ? "https" : "http"; // try the alternate one
                initialUriBuilder.Port = -1; // set to default port for scheme
                uri = initialUriBuilder.Uri;
                bool seenAsTwoSites = checkHTTPSRedirect(uri.ToString());
                if (seenAsTwoSites)
                {
                    // FAIL CHECK:
                    status = "bad";
                    meaningPrefix = $"Requesting the same URL using {initialUriBuilder.Scheme} results in no redirect back to the webpage with the specified URL scheme. Search engines would see this as a separate site. \n";
                    value = "HTTP and HTTPS versions are seen as separate sites";
                }
                else
                {
                    if(initialUriBuilder.Scheme == "http") // was https, redirects properly back to https when using http
                    {
                        status = "good";
                        meaningPrefix = "The URL entered eventually redirects to one using HTTPS.\n";
                        value = "HTTP redirects to HTTPS";
                    }
                    else // was http, redirects 'properly' back to http when using https
                    {
                        status = "warning";
                        meaningPrefix = "The URL entered eventually redirects to one using HTTP.\n";
                        value = "HTTPS redirects to HTTP";
                    }
                }
            }


            RankingFactor httpsRedirectFactor = new RankingFactor("Secure Redirection")
            {
                value = value,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Optimization:Mobile:Secure_Redirect"]
            };
            return httpsRedirectFactor;
        }

        /// <summary>method <c>canonicalRedirectFactor</c> returns a RankingFactor with a bad status a prefixed www. subdomain would appear as a different site AND a canonical tag is not set.</summary>
        public static RankingFactor canonicalRedirectFactor(IWebDriver driver, string url, string initialSanitizedURL)
        {
            string status = "good";
            string meaningPrefix = "";
            string value = "";
            string tagContent = "";
            try
            {
                IWebElement element = driver.FindElement(By.XPath("//link[@rel=\"canonical\"]"));
                tagContent = element.GetAttribute("href");
            }
            catch (NoSuchElementException)
            {
                tagContent = null; // probably unnecessary..
            }

            Uri uri = new UriBuilder(url).Uri;
            if (tagContent != null)
            {
                meaningPrefix = $"The canonical tag is set to link this page as '{tagContent}'.\n"; // assume if canonical tag set, is fine
                value = "Canonical tag set";
            }
            else
            {
                // CHECK
                string subdomain = GetSubDomain(uri);
                UriBuilder initialUriBuilder = new UriBuilder(initialSanitizedURL);
                if (subdomain == null || subdomain == "")
                {
                    initialUriBuilder.Host = "www." + uri.Host;
                    uri = initialUriBuilder.Uri;
                    int statuscode = checkFileStatusCode(uri.PathAndQuery);
                    if (statuscode == 301)
                    {
                        // FAIL CHECK:
                        status = "good";
                        meaningPrefix = $"Permanent redirect found. \n";
                        value = "301 Redirect";
                    }
                    else
                    {
                        // FAIL CHECK:
                        status = "bad";
                        meaningPrefix = $"No canonical tag or permanent redirect could be found. \n";
                        value = "None";
                    }
                }
                else if (subdomain == "www")
                {
                    var parts = initialUriBuilder.Host.Split('.');
                    initialUriBuilder.Host = parts[parts.Length - 2] + "." + parts[parts.Length - 1];
                    uri = initialUriBuilder.Uri;
                    int statuscode = checkFileStatusCode(uri.PathAndQuery);
                    if (statuscode == 301)
                    {
                        // FAIL CHECK:
                        status = "good";
                        meaningPrefix = $"Permanent redirect found. \n";
                        value = "301 Redirect";
                    }
                    else
                    {
                        // FAIL CHECK:
                        status = "bad";
                        meaningPrefix = $"No canonical tag or permanent redirect could be found. \n";
                        value = "None";
                    }
                }
                else
                {
                    // unneeded
                    status = "good";
                    meaningPrefix = $"No canonical tag or permanent redirect needed as this is a definitive subdomain. \n";
                    value = "None";
                }
            }

            RankingFactor hasCanonicalRedirect = new RankingFactor("Canonical Tag")
            {
                value = value,
                status = status,
                meaning = meaningPrefix + Startup.config["Factor_Meanings:Optimization:Indexation:Proper_Canonical_Redirect"]
            };
            return hasCanonicalRedirect;
        }

    }
}
