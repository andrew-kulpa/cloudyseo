﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class RankingFactor
    {
        public string title;
        public string xpath;
        public string value;
        public string meaning;
        public string status;

        /// <summary>Initializes a new instance of the <c>RankingFactor</c> class.</summary>
        public RankingFactor() { }

        /// <summary>Initializes a new instance of the <c>RankingFactor</c> class.</summary>
        public RankingFactor(string title)
        {
            this.title = title;
        }

        /// <summary>Initializes a new instance of the <c>RankingFactor</c> class.</summary>
        public RankingFactor(string title, string value)
        {
            this.title = title;
            this.value = value;
        }

        /// <summary>Initializes a new instance of the <c>RankingFactor</c> class.</summary>
        public RankingFactor(string title, string value, string meaning)
        {
            this.title = title;
            this.value = value;
            this.meaning = meaning;
        }

        /// <summary>Initializes a new instance of the <c>RankingFactor</c> class.</summary>
        public RankingFactor(string title, string value, string meaning, string xpath)
        {
            this.title = title;
            this.value = value;
            this.meaning = meaning;
            this.xpath = xpath;
        }
    }
}
