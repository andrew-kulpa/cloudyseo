﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class HeadingsFactor : MultivaluedRankingFactor
    {
        /// <summary>Initializes <c>HeadingsFactor</c> and uses base constructor of MultivaluedRankingFactor.</summary>
        public HeadingsFactor(string title, string meaning) : base(title, meaning)
        {

        }

        /// <summary>method <c>addHeading</c> adds the heading as a RankingFactor to a list of headings.</summary>
        public void addHeading(string heading, string content)
        {
            List<RankingFactor> headingFactors = new List<RankingFactor> { };
            headingFactors.Add(new RankingFactor("heading", heading) { status = "info" });
            headingFactors.Add(contentFactor(content));
            this.factorList.Add(headingFactors);
        }

        
        /// <summary>method <c>contentFactor</c> returns a RankingFactor setting the status to bad if there is the string is empty.</summary>
        public RankingFactor contentFactor(string content)
        {
            string status;
            if (content == null || content.Trim().Equals(""))
            {
                status = "bad";
            }
            else
            {
                status = "good";
            }
            return new RankingFactor("content", content)
            {
                status = status
            };
        }
    }
}
