﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class ImagesFactor : MultivaluedRankingFactor
    {

        /// <summary>Initializes <c>ImagesFactor</c> and uses base constructor of MultivaluedRankingFactor.</summary>
        public ImagesFactor(string title, string meaning) : base(title, meaning)
        {

        }

        
        /// <summary>method <c>addImage</c> adds the image data as a RankingFactor to a list of image data.</summary>
        public void addImage(string alt, string title, string url, int statusCode)
        {
            List<RankingFactor> imageFactors = new List<RankingFactor> { };
            imageFactors.Add(altFactor(alt));
            imageFactors.Add(urlFactor(url));
            imageFactors.Add(statusCodeFactor(statusCode));
            imageFactors.Add(titleFactor(title));
            this.factorList.Add(imageFactors);
        }
        
        /// <summary>method <c>altFactor</c> returns a RankingFactor setting the status to bad if the string is empty.</summary>
        public RankingFactor altFactor(string alt)
        {
            string status;
            if (alt == null || alt.Trim().Equals(""))
            {
                status = "bad";
            } else
            {
                status = "good";
            }
            return new RankingFactor("alt", alt)
            {
                status = status
            };
        }
        
        /// <summary>method <c>urlFactor</c> returns an informational RankingFactor.</summary>
        public RankingFactor urlFactor(string url)
        {
            string status = "info";
            string value = url;
            return new RankingFactor("url", value)
            {
                status = status
            };
        }
        
        /// <summary>method <c>titleFactor</c>returns a RankingFactor with a status set to bad if the string is empty.</summary>
        public RankingFactor titleFactor(string title)
        {
            string status;
            if (title == null || title.Trim().Equals(""))
            {
                status = "bad";
            }
            else
            {
                status = "good";
            }
            return new RankingFactor("title", title)
            {
                status = status
            };
        }
        
        /// <summary>method <c>statusCodeFactor</c> returns a RankingFactor with a status of good if 200, warning if 300-399, and bad otherwise.</summary>
        public RankingFactor statusCodeFactor(int statusCode)
        {
            string status;
            if (statusCode == 200)
            {
                status = "good";
            }
            else if (statusCode > 299 && statusCode < 400)
            {
                status = "warning";
            }
            else
            {
                status = "bad";
            }
            return new RankingFactor("status code", statusCode.ToString())
            {
                status = status
            };
        }
    }
}
