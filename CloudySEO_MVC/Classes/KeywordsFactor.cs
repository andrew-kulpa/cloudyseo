﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudySEO_MVC.Classes
{
    public class KeywordsFactor : MultivaluedRankingFactor
    {
        /// <summary>Initializes <c>KeywordsFactor</c> and uses base constructor of MultivaluedRankingFactor.</summary>
        public KeywordsFactor(string title, string meaning) : base(title, meaning)
        {

        }
        
        /// <summary>method <c>addKeyword</c> adds the keyword as a RankingFactor to a list of keywords.</summary>
        public void addKeyword(string keyword, int count)
        {
            List<RankingFactor> keywordFactors = new List<RankingFactor> { };
            keywordFactors.Add(new RankingFactor("keyword", keyword) { status = "info" });
            keywordFactors.Add(new RankingFactor("count", count.ToString()) { status = "info" });
            this.factorList.Add(keywordFactors);
        }
        
    }
}
