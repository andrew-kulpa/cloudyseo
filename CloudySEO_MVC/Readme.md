﻿# CloudySEO

A ASP.NET Core web application for analyzing webpages to objectively improve search engine rankings.

## Getting Started

First clone the repository and update the `appsettings(\.[A-Za-z])*.json` files to your desired specifications.

### Prerequisites

This tool requires a web server or debugging environment that can run ASP.NET Core App SDK version 2.0.0



## Deployment

On Ubuntu 16.04:
  
  1. Install .NET Core Runtime
  2. Clone repository
  3. `$ dotnet publish --configuration Release`
  4. `$ dotnet <app_assembly>.dll`
  5. Navigate to `http://<serveraddress>:<port>` and check if it is running properly


## Built With

* HtmlAgilityPack
* AspNetCore
* VisualStudio
* Robots.txt
* Selenium.Chrome.WebDriver
* Selenium.WebDriver

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Authors

* **Andrew Kulpa** - *Initial work* - [Andrew-Kulpa](https://github.com/Andrew-Kulpa)

See also the list of contributors who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
