﻿// Write your JavaScript code.

// based on https://stackoverflow.com/a/5717133/7867847
function validateUrl() {
    var str = document.forms['urlForm']['url'].value;
    if (str.match(/(http:\/\/|https:\/\/).*/g)) {
        return true;
    }
    else if (str.match(/.*:\/\/.*/g)) {
        alert("invalid protocol");
        return false;
    } else {
        return true;
    }
}
