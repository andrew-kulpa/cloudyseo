﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using CloudySEO_MVC.Classes;

namespace CloudySEO_MVC.Controllers
{
    public class AnalyzeController : Controller
    {

        /// <summary>method <c>Index</c> returns either an error or an SEO report for the resolved url's webpage.</summary>
        public IActionResult Index(string url)
        {
            ViewData["url"] = url;
            List<Thread> threads = new List<Thread>();
            ConcurrentBag<RankingCategory> rankings = new ConcurrentBag<RankingCategory> { };
            if (url != null && Valid(url))
            {
                ViewData["Message"] = $"SEO Analysis of {url}";
                WebScraper scraper = new WebScraper(url);

                if (!scraper.HasFinalOKResponse())
                {
                    ViewData["ErrorMessage"] = "Did not receive final OK Response from Web Server. SEO Analysis was aborted early.";
                    ViewData["Rankings"] = rankings;
                    return View();
                } else if (!scraper.isTextHTMLContentType())
                {
                    ViewData["ErrorMessage"] = "The document requested could not be verified to be a webpage. If you are the website administrator, add the value 'text/html' header attribute 'content-type'";
                    ViewData["Rankings"] = rankings;
                    return View();
                }

                // multithread slower/intense checks
                Thread imagesThread = new Thread(() => {
                    rankings.Add(scraper.ImagesCheck());
                });
                threads.Add(imagesThread);
                imagesThread.Start();

                Thread optimizationThread = new Thread(() => {
                    rankings.Add(scraper.OptimizationCheck());
                });
                threads.Add(optimizationThread);
                optimizationThread.Start();

                Thread  linksThread = new Thread(() => {
                    rankings.Add(scraper.LinksCheck());
                });
                threads.Add(linksThread);
                linksThread.Start();

                foreach (Thread thread in threads)
                {
                    thread.Join();
                }
                rankings.Add(scraper.ContentCheck());

                ViewData["Rankings"] = rankings;
                ViewData["Description"] = $"SEO Analysis of {url}";
                ViewData["Keywords"] = $"SEO Analysis {url} cloudyseo report";
                ViewData["OtherMetaTags"] = "<meta name=\"robots\" content=\"noindex, nofollow\">";
                //ViewData["Description"]
                return View();
            }
            else
            {
                ViewData["ErrorMessage"] = "Invalid, disallowed, or missing url parameter.";
                ViewData["Rankings"] = rankings;
                return View();
            }
        }

        /// <summary>method <c>Valid</c> returns true if the url is formatted correctly and is allowed to be scanned based on various constraints.</summary>
        private bool Valid(string url)
        {
            bool isValid = true;
            try
            {
                Uri uri = new UriBuilder(url).Uri;
                string[] allowedSchemes = new string[] { "http", "https" };
                if (uri.IsLoopback || Url.IsLocalUrl(uri.ToString())|| !allowedSchemes.Contains(uri.Scheme) || Startup.config.GetSection("Disallowed_Domains").GetChildren().Select(p => p.Value).Contains(uri.Host.ToLower())) 
                {
                    isValid = false;
                }
            } catch
            {
                isValid = false;
            }

            return isValid;
        }
    }
}